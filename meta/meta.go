package meta

import (
	"golang.org/x/mod/semver"
)

var (
	Version string
)

func MajorVersion() string {
	return semver.Major(Version)
}
