SHELL := /bin/bash
VERSION = $(shell cat VERSION)
PACKAGE := gitlab.wikimedia.org/repos/qte/catalyst/prototype-api


GO_LDFLAGS = \
  "-X '$(PACKAGE)/meta.Version=$(VERSION)'"

download:
	go mod download

build:
	go build -v -ldflags=$(GO_LDFLAGS)