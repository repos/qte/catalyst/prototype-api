package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHealthy(t *testing.T) {
	router := setupRouter()
	rec := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/healthz", nil)
	router.ServeHTTP(rec, req)
	assert.Equal(t, 200, rec.Code)
}
