package openstack

import (
	"fmt"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"github.com/joho/godotenv"
	"gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/clients"
	"gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/webproxy"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/environment"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/utils"
	"log"
	"os"
	"regexp"
	"strings"
	"time"
)

const K8sApiConfigMarker = "k8s-config:"

type OpenStack struct {
	provider      *gophercloud.ProviderClient
	computeClient *gophercloud.ServiceClient
	proxyClient   *webproxy.Client
}

func NewInstance() *OpenStack {
	confPath := os.Getenv("OS_CONF_PATH")
	if confPath == "" {
		confPath = "/srv/app/openstack_base_config"
	}
	// Values OS_APPLICATION_CREDENTIAL_ID and OS_APPLICATION_CREDENTIAL_SECRET are expected to be already in the env
	err := godotenv.Load(confPath)
	utils.HandleError(err)

	openStack := new(OpenStack)

	opts, err := openstack.AuthOptionsFromEnv()
	utils.HandleError(err)

	openStack.provider, err = openstack.AuthenticatedClient(opts)
	utils.HandleError(err)

	region := os.Getenv("OS_REGION_NAME")
	openStack.computeClient, err = openstack.NewComputeV2(openStack.provider, gophercloud.EndpointOpts{
		Region: region,
	})
	utils.HandleError(err)

	openStack.proxyClient, err = clients.NewWebProxyClient(openStack.provider, gophercloud.EndpointOpts{
		Region: region,
	})
	utils.HandleError(err)

	return openStack
}

func (openStack *OpenStack) ProvisionVm(env *environment.Environment) {
	openStack.createServer(env)
	openStack.createWebProxies(env)
}

func (openStack *OpenStack) createServer(env *environment.Environment) {
	userData := fmt.Sprintf(`#!/usr/bin/env bash
		curl -sfL https://get.k3s.io | sh -
	
		echo -n %s
		cat /etc/rancher/k3s/k3s.yaml | base64 | tr -d '\n'
		echo
		`, K8sApiConfigMarker)
	createOpts := servers.CreateOpts{
		Name:      env.UniqueName(),
		ImageRef:  "5434ad9d-52c7-4770-99e7-2da5b0f6751f", // debian-12.0-bookworm
		FlavorRef: "55d5d90f-c5c6-44ff-bb8a-be7b077481cf", // g3.cores1.ram2.disk20
		Networks: []servers.Network{
			{UUID: "7425e328-560c-4f00-8e99-706f3fb90bb4"}, // lan-flat-cloudinstances2b
		},
		SecurityGroups: []string{
			"43b2c8c2-12b3-40d7-91f4-e9ba4192d6fc", // default
			"5494c776-e202-4270-a8f8-2cebbf8f4d76", // http
			"bfa7468b-724e-478a-806e-45086a9a352a", // k8s
		},
		UserData: []byte(userData),
	}
	server, err := servers.Create(openStack.computeClient, createOpts).Extract()
	utils.HandleError(err)
	env.VpcInfo.VpcId = server.ID

	err = servers.WaitForStatus(openStack.computeClient, env.VpcInfo.VpcId, "ACTIVE", 20)
	utils.HandleError(err)

	// TODO: deadline
	for {
		console := openStack.GetConsole(env, 10)
		re := regexp.MustCompile("root@" + env.UniqueName() + `:~#\s+$`)
		if re.MatchString(console) {
			break
		}
		time.Sleep(20 * time.Second)
	}

	// Get server details again so the IP address is populated
	server, err = servers.Get(openStack.computeClient, server.ID).Extract()
	utils.HandleError(err)

	addrNet := server.Addresses["lan-flat-cloudinstances2b"].([]interface{})
	env.VpcInfo.ServerIp = addrNet[0].(map[string]interface{})["addr"].(string)

	log.Printf("%s: VM created", env.UniqueName())
}

func (openStack *OpenStack) createWebProxies(env *environment.Environment) {
	for _, cmp := range env.Components {
		for _, ingress := range cmp.IngressUrls {
			result := openStack.proxyClient.CreateProxy(&webproxy.Proxy{
				Domain:   ingress,
				Backends: []string{"http://" + env.VpcInfo.ServerIp + ":80"},
			})
			utils.HandleError(result.Err)

			log.Printf(`%s: Created web proxy "%s"`, cmp.UniqueName(env), ingress)
		}
	}
}

func (openStack *OpenStack) GetConsole(env *environment.Environment, noLines int) string {
	var consoleReady = func(err error) bool {
		if err != nil {
			if strings.Contains(err.Error(), "409") {
				return false
			} else {
				utils.HandleError(err)
			}
		}
		return true
	}

	consoleOutputOpts := servers.ShowConsoleOutputOpts{
		Length: noLines,
	}
	console, err := servers.ShowConsoleOutput(openStack.computeClient, env.VpcInfo.VpcId, consoleOutputOpts).Extract()
	if !consoleReady(err) {
		for range [12]int{} { // Wait for a minute
			if !consoleReady(err) {
				time.Sleep(5 * time.Second)
				console, err = servers.ShowConsoleOutput(openStack.computeClient, env.VpcInfo.VpcId, consoleOutputOpts).Extract()
			}
			break
		}
		log.Fatal("Console could not be retrieved after a minute. Instance:", env.VpcInfo.VpcId)
	}

	return console
}
