package kubernetes

import (
	"fmt"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/environment"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/helm"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/utils"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/cli"
	"log"
	"maps"
	"os"
	"strings"
)

func DeployComponent(env *environment.Environment, cmp *environment.Component) {
	utils.WithRecovery(func() {
		settings := cli.New()
		settings.KubeConfig = env.K8sApiConfigPath()
		settings.SetNamespace("default")

		actionConfig := new(action.Configuration)
		err := actionConfig.Init(
			settings.RESTClientGetter(), settings.Namespace(), os.Getenv("HELM_DRIVER"), log.Printf,
		)
		utils.HandleError(err)

		chartPath, err := helm.GetChartPath(cmp.Name)
		utils.HandleError(err)

		cmpChart, err := loader.Load(chartPath)
		utils.HandleError(err)

		client := action.NewInstall(actionConfig)
		client.ReleaseName = cmp.UniqueName(env)
		client.Namespace = settings.Namespace()

		chartValues := prepareChartValues(cmp)
		applyValues(cmpChart, chartValues)

		rel, err := client.Run(cmpChart, cmpChart.Values)
		utils.HandleError(err)

		cmp.Status = "running"
		log.Println(fmt.Sprintf(`Created release "%s" for environment "%s"`, rel.Name, env.UniqueName()))
	}, func(msg any) {
		cmp.Status = "failed"
		panic(msg)
	})
}

func prepareChartValues(cmp *environment.Component) map[string]string {
	values := map[string]string{}

	// Customizations
	for _, custData := range cmp.Manifest.CustomizationLookup {
		values[custData.HelmVariable] = custData.Default
	}
	for name, val := range cmp.CustomizationsAsMap() {
		custData := cmp.Manifest.CustomizationLookup[name]
		values[custData.HelmVariable] = val
	}

	// Ingresses
	maps.Copy(values, cmp.Manifest.PopulatedIngresses)

	return values
}

func applyValues(chart *chart.Chart, customizations map[string]string) {
	assignChartValue := func(key string, val string) {
		var node map[string]interface{}
		nesting := strings.Split(key, ".")
		for _, nodeName := range nesting[:len(nesting)-1] {
			node = chart.Values[nodeName].(map[string]interface{})
		}

		node[nesting[len(nesting)-1]] = val
	}

	for key, val := range customizations {
		assignChartValue(key, val)
	}
}
