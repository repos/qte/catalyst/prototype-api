package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/environment"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/mariadb"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/meta"
	"net/http"
	"os"
	"strconv"
	"strings"
)

const PathPrefix = "/api/"

var (
	ClientSecret string
)

func extractToken(c *gin.Context) string {
	authHeader := c.Request.Header.Get("Authorization")
	bearerStrings := strings.Split(authHeader, " ")
	if len(bearerStrings) == 2 && bearerStrings[0] == "Bearer" {
		return bearerStrings[1]
	}
	return ""
}

func decodeJwt(tokenString string) (*jwt.Token, error) {
	return jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(ClientSecret), nil
	})
}

func isInCatalystGroup(token *jwt.Token) bool {
	claims, ok := token.Claims.(jwt.MapClaims)
	if token.Valid && ok {
		groups := claims["groups"].([]interface{})
		for _, group := range groups {
			if group == "project-catalyst" {
				return true
			}
		}
	}
	return false
}

func isAuthorized() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := extractToken(c)
		if tokenString != "" {
			token, err := decodeJwt(tokenString)
			if err != nil {
				c.AbortWithError(http.StatusUnauthorized, err)
			}
			if isInCatalystGroup(token) {
				c.Next()
			} else {
				c.AbortWithStatus(http.StatusUnauthorized)
			}
		} else {
			c.AbortWithStatus(http.StatusUnauthorized)
		}
	}
}

func pingDatabase(c *gin.Context) {
	err := DB.Ping()
	if err != nil {
		c.Status(200)
	} else {
		c.AbortWithError(http.StatusInternalServerError, err)
	}
}

func getEnvironments(c *gin.Context) {
	environments := mariadb.RetrieveEnvironments(DB)
	c.IndentedJSON(http.StatusOK, environments)
}

func getEnvironment(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
	}
	env, err := mariadb.RetrieveEnvironment(DB, id)
	if err != nil {
		c.AbortWithError(http.StatusNotFound, err)
	} else {
		c.IndentedJSON(http.StatusOK, env)
	}
}

func postEnvironment(c *gin.Context) {
	var postEnvironment environment.Environment
	if err := c.BindJSON(&postEnvironment); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	if err := Validate(&postEnvironment); err != nil {
		c.AbortWithError(http.StatusUnprocessableEntity, err)
		return
	}

	CreateEnvironmentAsync(&postEnvironment)
	c.JSON(http.StatusAccepted, postEnvironment)
}

func setupRouter() *gin.Engine {
	apiVersionPrefix := PathPrefix + meta.MajorVersion()
	router := gin.Default()
	router.GET("/healthz", func(c *gin.Context) { c.Status(200) })
	router.GET("/ready", pingDatabase)
	router.Use(isAuthorized())
	router.GET(apiVersionPrefix+"/environments", getEnvironments)
	router.GET(apiVersionPrefix+"/environment/:id", getEnvironment)
	router.POST(apiVersionPrefix+"/environments", postEnvironment)
	return router
}

func RunRouter(address string) {
	ClientSecret = os.Getenv("CLIENT_SECRET")
	router := setupRouter()
	router.Run(address)
}
