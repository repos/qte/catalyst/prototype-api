package main

import (
	"database/sql"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/backend/openstack"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/mariadb"
	"log"
	"os"
)

var (
	DB        *sql.DB
	OpenStack *openstack.OpenStack
)

func main() {
	// By default `log` prints to StdErr
	log.SetOutput(os.Stdout)

	DB = mariadb.SetupDb()
	defer DB.Close()
	OpenStack = openstack.NewInstance()

	RunRouter("0.0.0.0:8080")
}
