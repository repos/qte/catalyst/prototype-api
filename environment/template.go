package environment

import (
	"bytes"
	"fmt"
	"text/template"
)

// TODO limit what info can be bound. We don't want to leak sensitive stuff

func (env *Environment) ParseTmpl(tmplVal string) (string, error) {
	wrap := struct {
		Catalyst struct {
			Environment *Environment
		}
	}{
		Catalyst: struct {
			Environment *Environment
		}{Environment: env},
	}

	nonSolvableError := fmt.Errorf(`specified template "%s" cannot be processed`, tmplVal)
	tmpl, err := template.New("Solver").Parse(tmplVal)
	if err != nil {
		return "", nonSolvableError
	}
	var solved bytes.Buffer
	err = tmpl.Execute(&solved, wrap)
	if err != nil {
		return "", nonSolvableError
	}

	return solved.String(), nil
}
