package environment

import (
	"errors"
	"fmt"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/helm"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/utils"
	"gopkg.in/yaml.v3"
	"os"
)

type Ingress struct {
	HostName     string `yaml:"hostName"`
	HelmVariable string `yaml:"helmVariable"`
}

type Customization struct {
	Name         string `yaml:"name"`
	Label        string `yaml:"label"`
	HelmVariable string `yaml:"helmVariable"`
	Default      string `yaml:"default"`
	Type         string `yaml:"type"`
}

type Spec struct {
	Ingresses      []Ingress       `yaml:"ingresses"`
	Customizations []Customization `yaml:"customizations"`
}

type CatalystManifest struct {
	Spec                Spec `yaml:"spec"`
	CustomizationLookup map[string]Customization
	PopulatedIngresses  map[string]string
}

func (man *CatalystManifest) PopulateCaches(env *Environment) {
	man.populateCustomizationLookup()
	man.populateInterpolatedIngresses(env)
}

func (man *CatalystManifest) populateCustomizationLookup() {
	custLookup := map[string]Customization{}
	for _, cust := range man.Spec.Customizations {
		custLookup[cust.Name] = cust
	}
	man.CustomizationLookup = custLookup
}

func (man *CatalystManifest) populateInterpolatedIngresses(env *Environment) {
	var errs []error
	ingresses := map[string]string{}

	for _, ingressSpec := range man.Spec.Ingresses {
		if solvedHostname, err := env.ParseTmpl(ingressSpec.HostName); err == nil {
			ingress := fmt.Sprintf("catalyst-%s%s", solvedHostname, os.Getenv("CLOUD_DOMAIN"))
			ingresses[ingressSpec.HelmVariable] = ingress

		} else {
			errs = append(errs, err)
		}
	}
	resErr := errors.Join(errs...)
	utils.HandleError(resErr)

	man.PopulatedIngresses = ingresses
}

func (cmp *Component) parseManifest() {
	manifest := CatalystManifest{}

	chartPath, err := helm.GetChartPath(cmp.Name)
	utils.HandleError(err)
	rawManifest, err := os.ReadFile(chartPath + "/catalyst.yaml")
	utils.HandleError(err)
	err = yaml.Unmarshal(rawManifest, &manifest)
	utils.HandleError(err)

	cmp.Manifest = &manifest
}
