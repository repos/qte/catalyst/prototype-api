package environment

import (
	"encoding/json"
	"fmt"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/utils"
	"strings"
	"time"
)

//TODO: separate API model from persistence model

type Environment struct {
	Id         int64       `json:"id"`
	Name       string      `json:"name" binding:"required"`
	CreatedAt  time.Time   `json:"created_at"` // UTC
	VpcInfo    VpcInfo     `json:"-"`
	Components []Component `json:"components" binding:"required,min=1"`
}

func (env *Environment) Init() {
	if env.Id == 0 {
		panic(
			fmt.Sprintf(`cannot init environment "%s". Id has not been asigned`, env.Name),
		)
	}

	env.Name = strings.ToLower(env.Name)
	env.CreatedAt = time.Now().UTC()

	var initComps []Component
	for _, cmp := range env.Components {
		cmp.Init(env)
		initComps = append(initComps, cmp)
	}
	env.Components = initComps
}

func (env *Environment) K8sApiConfigPath() string {
	return fmt.Sprintf("/tmp/k8s%d", env.Id)
}

func (env *Environment) UniqueName() string {
	return fmt.Sprintf("%s-%d", env.Name, env.Id)
}

type VpcInfo struct {
	VpcId        string
	ServerIp     string
	K8sApiConfig string
}

type Component struct {
	Id             int64             `json:"id"`
	Type           string            `json:"type" binding:"required"`
	Name           string            `json:"name" binding:"required"`
	Status         string            `json:"status"`
	IngressUrls    []string          `json:"ingressUrls"`
	Customizations json.RawMessage   `json:"customizations"`
	Manifest       *CatalystManifest `json:"-"`
}

func (cmp *Component) Init(env *Environment) {
	cmp.Status = "starting"
	cmp.parseManifest()
	cmp.Manifest.PopulateCaches(env)

	for _, ingressUrl := range cmp.Manifest.PopulatedIngresses {
		cmp.IngressUrls = append(cmp.IngressUrls, ingressUrl)
	}
}

func (cmp *Component) UniqueName(env *Environment) string {
	return fmt.Sprintf("%s-%s", env.UniqueName(), cmp.Name)
}

func (cmp *Component) CustomizationsAsMap() map[string]string {
	cstMap := map[string]string{}

	if cmp.Customizations == nil {
		return cstMap
	}
	err := json.Unmarshal(cmp.Customizations, &cstMap)
	utils.HandleError(err)
	return cstMap
}
