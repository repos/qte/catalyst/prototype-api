package helm

import (
	"fmt"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/utils"
	"os"
)

func ChartExists(name string) (bool, error) {
	_, err := GetChartPath(name)
	return err == nil, err
}

func GetChartPath(name string) (string, error) {
	chartsLocation := os.Getenv("CHARTS_LOCATION")
	if chartsLocation == "" {
		chartsLocation = "/srv/app/charts/"
	}
	chartPath := chartsLocation + name
	if utils.FileExists(chartPath) {
		return chartPath, nil
	}
	return "", fmt.Errorf(`unknown chart "%s"`, name)
}
