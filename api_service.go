package main

import (
	"encoding/base64"
	"errors"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/backend/kubernetes"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/backend/openstack"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/environment"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/helm"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/mariadb"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/utils"
	"os"
	"regexp"
	"strings"
)

func Validate(env *environment.Environment) error {
	// TODO: validate no repeated charts
	var errs []error
	for _, cmp := range env.Components {
		if ok, err := helm.ChartExists(cmp.Name); !ok {
			errs = append(errs, err)
		}
	}
	return errors.Join(errs...)
}

func CreateEnvironmentAsync(env *environment.Environment) {
	// Insert environment first so id gets populated
	mariadb.InsertEnvironment(DB, env)
	env.Init()
	mariadb.UpdateEnvironment(DB, env)

	handleAsync(func() {
		utils.WithRecovery(func() {
			OpenStack.ProvisionVm(env)
			getK8sApiConfig(env)
			for i := 0; i < len(env.Components); i++ {
				kubernetes.DeployComponent(env, &env.Components[i])
			}
		}, func(msg any) {
			// TODO: Clean up here
			utils.LogWithStackTrace(msg)
		})
		mariadb.UpdateEnvironment(DB, env)
	})
}

func getK8sApiConfig(env *environment.Environment) {
	var extractConfFromInstance = func() string {
		console := OpenStack.GetConsole(env, 75)

		re := regexp.MustCompile(openstack.K8sApiConfigMarker + `(?P<K8sConf>.*)\n`)
		matches := re.FindStringSubmatch(console)
		encodedK3sConf := matches[re.SubexpIndex("K8sConf")]
		decodedK3sConf, _ := base64.StdEncoding.DecodeString(encodedK3sConf)
		return string(decodedK3sConf)
	}

	// The image we are using to run the api is built with blubber, which runs with user `runuser`. This user has very
	// limited permissions and can't even write to `/srv/app` (let alone `/var/lib`). We place these files in `/tmp/`
	// for now
	var k8sConf string
	k8sConfPath := env.K8sApiConfigPath()
	if !utils.FileExists(k8sConfPath) {
		k8sConf = extractConfFromInstance()
		serverK8sApi := "https://" + env.VpcInfo.ServerIp + ":6443"
		k8sConfIp := strings.Replace(k8sConf, "https://127.0.0.1:6443", serverK8sApi, 1)

		env.VpcInfo.K8sApiConfig = k8sConfIp
		err := os.WriteFile(k8sConfPath, []byte(k8sConfIp), 0600)
		utils.HandleError(err)
	}
}

func handleAsync(f func()) {
	go func() {
		utils.WithRecovery(f, utils.LogWithStackTrace)
	}()
}
