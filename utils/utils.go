package utils

import (
	"errors"
	"log"
	"os"
	"runtime/debug"
)

var StdErrLog = log.New(os.Stderr, "", 0)

func LogWithStackTrace(msg any) {
	StdErrLog.Println(msg, ":\n", string(debug.Stack()))
}

func HandleError(err error) {
	if err != nil {
		panic(err)
	}
}

func WithRecovery(f func(), callback func(v any)) {
	defer func() {
		if r := recover(); r != nil {
			callback(r)
		}
	}()

	f()
}

func FileExists(file string) bool {
	_, err := os.Stat(file)
	return !errors.Is(err, os.ErrNotExist)
}
