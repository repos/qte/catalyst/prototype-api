package mariadb

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/environment"
	"gitlab.wikimedia.org/repos/qte/catalyst/prototype-api/utils"
	"log"
	"os"
	"strings"
)

var (
	username string
	password string
	host     string
	database string
)

func createTables(db *sql.DB) {
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS environments (
    	id integer NOT NULL AUTO_INCREMENT,
    	name varchar(100) NOT NULL,
    	created_at DATETIME NOT NULL,
    	PRIMARY KEY (id)
		)`)
	utils.HandleError(err)

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS vpc_info(
    	id integer NOT NULL AUTO_INCREMENT,
    	vpc_id varchar(36) NOT NULL,
    	server_ip varchar(15) NOT NULL,
    	k8s_api_config blob NOT NULL,
    	env_id integer NOT NULL REFERENCES environments(id),
    	PRIMARY KEY (id),
    	UNIQUE(env_id)
		)`)
	utils.HandleError(err)

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS components(
    	id integer NOT NULL AUTO_INCREMENT,
    	type varchar(100) NOT NULL,
    	name varchar(100) NOT NULL,
    	status varchar(20) NOT NULL,
    	ingress_urls varchar(500) NOT NULL,
    	customizations JSON,
    	env_id integer NOT NULL REFERENCES environments(id),
    	PRIMARY KEY (id),
    	UNIQUE (env_id, name),
    	CHECK (JSON_VALID(customizations))
		)`)
	utils.HandleError(err)
}

func SetupDb() *sql.DB {
	username = os.Getenv("DB_USER")
	password = os.Getenv("DB_PASS")
	host = os.Getenv("DB_HOST")
	database = os.Getenv("DB_DATABASE")
	dsn := username + ":" + password + "@tcp(" + host + ":3306)/" + database + "?parseTime=true"
	db, err := sql.Open("mysql", dsn)
	utils.HandleError(err)

	log.Println("Connected to mariadb")
	createTables(db)
	return db
}

func RetrieveComponents(db *sql.DB, envId int64) []environment.Component {
	var components []environment.Component
	res, err := db.Query(
		`SELECT id, type, name, status, ingress_urls, COALESCE(customizations, 'null') FROM components WHERE env_id = ?`,
		envId,
	)
	defer res.Close()
	utils.HandleError(err)
	for res.Next() {
		var component = environment.Component{}
		var urlsRaw string
		err = res.Scan(
			&component.Id, &component.Type, &component.Name, &component.Status, &urlsRaw, &component.Customizations,
		)
		utils.HandleError(err)
		if strings.TrimSpace(urlsRaw) == "" {
			component.IngressUrls = []string{}
		} else {
			component.IngressUrls = strings.Split(urlsRaw, ",")
		}

		components = append(components, component)
	}
	return components
}

func RetrieveEnvironment(db *sql.DB, id int64) (*environment.Environment, error) {
	selector := fmt.Sprintf("WHERE environments.id = %d", id)
	envs := RetrieveEnvironments(db, selector)
	if len(envs) > 0 {
		return &envs[0], nil
	}
	return nil, errors.New(fmt.Sprintf(`Environment with id "%d" does not exist`, id))
}

func RetrieveEnvironments(db *sql.DB, selector ...string) []environment.Environment {
	query := "SELECT * FROM environments JOIN vpc_info ON environments.id = vpc_info.env_id"
	if len(selector) > 0 {
		query += " " + selector[0]
	}

	var environments []environment.Environment
	res, err := db.Query(query)
	defer res.Close()
	utils.HandleError(err)
	for res.Next() {
		var env = environment.Environment{}
		env.VpcInfo = environment.VpcInfo{}
		var throwAway int
		err = res.Scan(
			&env.Id, &env.Name, &env.CreatedAt, &throwAway, &env.VpcInfo.VpcId, &env.VpcInfo.ServerIp,
			&env.VpcInfo.K8sApiConfig, &throwAway,
		)
		utils.HandleError(err)
		env.Components = RetrieveComponents(db, env.Id)

		environments = append(environments, env)
	}
	return environments
}

//TODO: Add transactions to insert/update functions

func InsertComponents(db *sql.DB, envId int64, components []environment.Component) []environment.Component {
	var inserted []environment.Component
	for _, component := range components {
		res, err := db.Exec(
			`INSERT INTO components VALUES (default, ?, ?, ?, ?, ?, ?)`,
			component.Type, component.Name, component.Status, strings.Join(component.IngressUrls, ","),
			component.Customizations, envId,
		)
		utils.HandleError(err)
		id, err := res.LastInsertId()
		component.Id = id
		inserted = append(inserted, component)
	}

	return inserted
}

func InsertEnvironment(db *sql.DB, env *environment.Environment) {
	res, err := db.Exec(`INSERT INTO environments VALUES (default, ?, ?)`, env.Name, env.CreatedAt)
	utils.HandleError(err)
	id, errId := res.LastInsertId()
	utils.HandleError(errId)
	env.Id = id
	env.Components = InsertComponents(db, id, env.Components)

	vpcInfo := env.VpcInfo
	_, err = db.Exec(
		`INSERT INTO vpc_info VALUES (default, ?, ?, ?, ?)`,
		vpcInfo.VpcId, vpcInfo.ServerIp, vpcInfo.K8sApiConfig, env.Id,
	)
	utils.HandleError(err)
}

func UpdateComponents(db *sql.DB, components []environment.Component) {
	for _, component := range components {
		_, err :=
			db.Exec(
				`UPDATE components SET status=?, ingress_urls=?, customizations=? WHERE id=?`,
				component.Status, strings.Join(component.IngressUrls, ","), component.Customizations, component.Id,
			)
		utils.HandleError(err)
	}
}

func UpdateEnvironment(db *sql.DB, env *environment.Environment) {
	// Environments table itself doesn't currently have fields that need updating
	vpcInfo := env.VpcInfo
	_, err := db.Exec(
		`UPDATE vpc_info SET vpc_id=?, server_ip=?, k8s_api_config=? WHERE env_id=?`,
		vpcInfo.VpcId, vpcInfo.ServerIp, vpcInfo.K8sApiConfig, env.Id,
	)
	utils.HandleError(err)

	UpdateComponents(db, env.Components)
}
